import React, { useEffect, useState } from "react";
import ButtonWithSpinner from "../buttons/button-with-spinner";
import { useNavigate, useParams } from "react-router-dom";
import { useSelector } from "react-redux";
import ServicesRepo from "../../api-services/services-repo";
import Spinner from "../spinners/spinner.js";


const ServiceAddEdit = () => {

    const navigate = useNavigate();
    const {id: serviceId} = useParams();
    const servicesRepo = new ServicesRepo();
    const [editingItem, setEditingItem] = useState();
    const {item, loading, error, editingError} = useSelector((store) => store.serviceEditReducer);


    useEffect(() => {
        servicesRepo.getServiceById(serviceId);
    }, [])

    useEffect(() => {
        setEditingItem(item)
    }, [item])



    const handleChange = ({target}) => {
        const name = target.name;
        const value = target.value;
        setEditingItem(prevItem => ({...prevItem, [name]: value}));
    }


    const handleSubmit = async(evt) => {
        evt.preventDefault();
        await servicesRepo.postService(editingItem);
        if (!editingError)
            navigate("/");    
    }


    const handleCancel = () => {
        navigate("/");
    }



    if (loading)
        return <div><Spinner size={30}/></div>
    
    if (error) 
        return <div>Произошла ошибка</div>

    if (editingItem)
        return (
            <form onSubmit={handleSubmit}>
                
                {editingError && <div>Произошла ошибка сохранения</div>}
                
                <div>
                    <div><label htmlFor="name">Название</label></div>
                    <div>
                        <input  type="text"
                                name="name"
                                id="name"
                                value={editingItem.name}
                                onChange={handleChange}/>
                    </div>
                    
                    <div><label htmlFor="price">Цена</label></div>
                    <div>
                        <input type="text"
                            name="price"
                            id="price"
                            value={editingItem.price}
                            onChange={handleChange}>
                        </input>
                    </div>

                    <div><label htmlFor="content">Описание</label></div>
                    <div>
                        <input type="text"
                            name="content"
                            id="content"
                            value={editingItem.content}
                            onChange={handleChange}>
                        </input>
                    </div>

                    <div className="buttons-row">
                        <ButtonWithSpinner width="20px" 
                                           text="cancel" 
                                           size={10} 
                                           onClick={handleCancel}/>

                        <ButtonWithSpinner width="40px" 
                                           text="save" 
                                           size={10} 
                                           isSubmit={true}/>
                    </div>
                </div>
            </form>
        )
    else
        return null;

}

export default ServiceAddEdit;