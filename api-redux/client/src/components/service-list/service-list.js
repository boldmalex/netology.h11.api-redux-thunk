import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import Spinner from "../spinners/spinner.js";
import ButtonWithSpinner from "../buttons/button-with-spinner.js";
import ServicesRepo from "../../api-services/services-repo.js";
import { useNavigate } from "react-router-dom";



const ServiceList = () => {

    const {items, busyItems, loading, error} = useSelector((store) => store.serviceListReducer);
    const servicesRepo = new ServicesRepo();
    const navigate = useNavigate();


    useEffect(() => {
        servicesRepo.getAllServices();
    }, []);


    // Обработчик кнопки Удалить
    const handleRemove = async(id) => {
        await servicesRepo.removeService(id)
        servicesRepo.getAllServices();
    };


    // Обработчик кнопки Редактировать
    const handleChange = (id) => {
       navigate(`services/${id}`);
    };


    // Визуализация одного элемента
    const showItem = (item) => {
        return(
            <li key={item.id} className={busyItems.some(busyId => item.id === busyId) ? "item-disabled" : ""}>
                    
                    {item.name} : {item.price}

                    <ButtonWithSpinner spinnerSize={10} 
                                       width="20px" 
                                       text='X'
                                       onClick={() => handleRemove(item.id)}/>

                    <button onClick={() => handleChange(item.id)}>
                        edit
                    </button>

            </li>
        );
    }



    // -- Render block --
    // ------------------

    if (loading)
        return <div><Spinner size={30}/></div>
    
    if (error) 
        return <div>Произошла ошибка!</div>

    if (items && items.length > 0)
        return (
                <ul>
                    {items.map(item => showItem(item))}
                </ul>
        )
    else
        return null;
}


export default ServiceList;