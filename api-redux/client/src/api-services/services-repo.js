import ServicesApiService from "./services-api-service";
import { useDispatch } from "react-redux";
import serviceActionTypes from "../store/service-action-types";


const api = new ServicesApiService();

class ServicesRepo {
    
    dispatch = useDispatch();

    async getAllServices() {
        
        this.dispatch({type: serviceActionTypes.FETCH_SERVICES_REQUEST});

        return await api.getAsync()
                        .then(response => {
                            this.dispatch({type: serviceActionTypes.FETCH_SERVICES_SUCCESS, payload:{items: response}});
                        })                   
                        .catch(error => {
                            this.dispatch({type: serviceActionTypes.FETCH_SERVICES_FAILURE, payload: {error: error}});
                        })
    }


    async getServiceById(id) {
        
        this.dispatch({type: serviceActionTypes.FETCH_SERVICE_REQUEST});

        return await api.getByIdAsync(id)
                        .then(response => {
                            this.dispatch({type: serviceActionTypes.FETCH_SERVICE_SUCCESS, payload:{item: response}});
                        })                   
                        .catch(error => {
                            this.dispatch({type: serviceActionTypes.FETCH_SERVICE_FAILURE, payload: {error: error}});
                        })
    }


    async postService(service) {
        
        return await api.postAsync(service)
                        .then(response => {
                            this.dispatch({type: serviceActionTypes.FETCH_SERVICE_SUCCESS, payload:{item: response}});
                        })                   
                        .catch(error => {
                            this.dispatch({type: serviceActionTypes.EDIT_SERVICE_FAILURE, payload: {error: error}});
                        })
    }

    
    async removeService(id){
        
        this.dispatch({type: serviceActionTypes.SET_SERVICE_AS_BUSY, payload: {busyId: id}});

        return await api.removeAsync(id)
                        .catch(error => {
                            this.dispatch({type: serviceActionTypes.FETCH_SERVICES_FAILURE, payload: {error: error}});
                        })
    }
}

export default ServicesRepo;