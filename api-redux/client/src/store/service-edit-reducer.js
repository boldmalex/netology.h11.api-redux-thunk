import serviceActionTypes from "./service-action-types";

const initialState = {
    item: {
        name: '',
        price: '',
        content: ''
    }, 
    loading: false,
    error: null,
    editingError: null
};

const serviceEditReducer = (state = initialState, action) => {

    switch(action.type) {
        
        case serviceActionTypes.FETCH_SERVICE_REQUEST: 
            return {...state, loading: true, error: null, editingError: null};

        case serviceActionTypes.FETCH_SERVICE_FAILURE:
            const {error} = action.payload;
            return {...state, loading: false, error, editingError: null};

        case serviceActionTypes.FETCH_SERVICE_SUCCESS:
            const {item} = action.payload;
            return {...state, item, loading: false, error: null, editingError: null};

        case serviceActionTypes.EDIT_SERVICE_FAILURE: 
            const {editingError} = action.payload;
            return {...state, loading: false, error: null, editingError: editingError};
        
        default: return state;
    }
}


export default serviceEditReducer;