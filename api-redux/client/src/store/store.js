import serviceEditReducer from "./service-edit-reducer";
import serviceListReducer from "./service-list-reducer";
import { combineReducers, createStore } from "redux";

const reducer = combineReducers({
    serviceEditReducer,
    serviceListReducer
});

const store = createStore(reducer);

export default store;
