import react from "react";
import { Provider } from "react-redux";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import ServiceList from "./components/service-list/service-list";
import ServiceAddEdit from "./components/service-add-edit/service-add-edit";
import store from "./store/store.js";
import "./app.css";


function App() {
  return (
    <Provider store = {store}>
      <Router>
        <Routes>
          <Route path="/" element={<ServiceList/>}/>
          <Route path="services/:id" element={<ServiceAddEdit/>}/>
        </Routes>
      </Router>
    </Provider>
  );
}


export default App;
