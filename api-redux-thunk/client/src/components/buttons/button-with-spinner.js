import React, { useState } from "react";
import Spinner from "../spinners/spinner.js";


const ButtonWithSpinner = (props) => {

    const {text, width="20px", spinnerSize = 10, onClick, isSubmit} = props;
    const [isShowSpinner, setShowSpinner] = useState(false);
    
    const onButtonClick = () => {
        setShowSpinner(!isShowSpinner);
        onClick && onClick();
    }

    const showTextOrSpinner = () => {
        
        if (isShowSpinner)
            return <Spinner size={spinnerSize}/>;
        else
            return text;
    }

    return (
        <>
            <button width={width} 
                    type={isSubmit ? "submit" : "button"} 
                    onClick={onButtonClick}>
                    {showTextOrSpinner()}
            </button> 
        </>
    )
}

export default ButtonWithSpinner;