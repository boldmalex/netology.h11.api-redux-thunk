import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import Spinner from "../spinners/spinner.js";
import ButtonWithSpinner from "../buttons/button-with-spinner.js";
import {getAllServices, removeService} from "../../store/action-creators.js"
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";



const ServiceList = () => {

    const dispatch = useDispatch();

    const {items, busyItems, loading, error} = useSelector((store) => store.serviceListReducer);
    const navigate = useNavigate();


    useEffect(() => {
        dispatch(getAllServices());
    }, []);


    // Обработчик кнопки Удалить
    const handleRemove = async(id) => {
        await dispatch(removeService(id));
        dispatch(getAllServices());
    };


    // Обработчик кнопки Редактировать
    const handleChange = (id) => {
       navigate(`services/${id}`);
    };


    // Визуализация одного элемента
    const showItem = (item) => {
        return(
            <li key={item.id} className={busyItems.some(busyId => item.id === busyId) ? "item-disabled" : ""}>
                    
                    {item.name} : {item.price}

                    <ButtonWithSpinner spinnerSize={10} 
                                       width="20px" 
                                       text='X'
                                       onClick={() => handleRemove(item.id)}/>

                    <button onClick={() => handleChange(item.id)}>
                        edit
                    </button>

            </li>
        );
    }



    // -- Render block --
    // ------------------

    if (loading)
        return <div><Spinner size={30}/></div>
    
    if (error) 
        return <div>Произошла ошибка!</div>

    if (items && items.length > 0)
        return (
                <ul>
                    {items.map(item => showItem(item))}
                </ul>
        )
    else
        return null;
}


export default ServiceList;