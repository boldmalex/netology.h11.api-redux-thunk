import React from "react";
import { SpinnerCircularSplit } from "spinners-react";

const Spinner = (props) => {

    const {size = 10} = props;

    return(
        <>
            <SpinnerCircularSplit size={size} thickness={100} speed={100} color="rgba(83, 57, 172, 1)" secondaryColor="rgba(57, 167, 172, 0.44)" />
        </>
    )
}

export default Spinner;