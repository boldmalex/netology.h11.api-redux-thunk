const url = "http://localhost:7070/api/services";

class ServicesApiService {
    
    async getAsync() {
        const response = await fetch(url);
        if (response.ok)
            return await response.json();
        else
            throw new Error(`get fetch error ${url}, received: ${response.status}`);
    }


    async getByIdAsync(id) {
        
        const fetchUrl = `${url}/${id}`;
        
        const response = await fetch(fetchUrl);

        if (response.ok)
            return await response.json();
        else
            throw new Error(`get fetch error ${fetchUrl}, received: ${response.status}`);

    }


    async postAsync(service) {
        
        const response = await fetch(url, {
                                    method: "POST",
                                    body: JSON.stringify(service),
                                    headers: {
                                        "Content-Type":"application/json"
                                    }
                                });
        if (!response.ok)
            throw new Error(`post fetch error ${url}, received: ${response.status}`);
    }


    async removeAsync(id) {
        
        const fetchUrl = `${url}/${id}`

        const response = await fetch(fetchUrl, {method: "DELETE"});

        if (!response.ok)
            throw new Error(`delete fetch error ${url}, received: ${response.status}`);
    }
}


export default ServicesApiService;