import serviceActionTypes from "./service-action-types";
import ServicesApiService from "../api-services/services-api-service";


const api = new ServicesApiService();


export const getAllServices = () => (dispatch, getState) => {
    
    dispatch({type: serviceActionTypes.FETCH_SERVICES_REQUEST});

    return api.getAsync()
                    .then(response => {
                        dispatch({type: serviceActionTypes.FETCH_SERVICES_SUCCESS, payload:{items: response}});
                    })                   
                    .catch(error => {
                        dispatch({type: serviceActionTypes.FETCH_SERVICES_FAILURE, payload: {error: error}});
                    })

}


export const getServiceById = (id) => async(dispatch, getState) => {
        
    dispatch({type: serviceActionTypes.FETCH_SERVICE_REQUEST});

    await api.getByIdAsync(id)
                    .then(response => {
                        dispatch({type: serviceActionTypes.FETCH_SERVICE_SUCCESS, payload:{item: response}});
                    })                   
                    .catch(error => {
                        dispatch({type: serviceActionTypes.FETCH_SERVICE_FAILURE, payload: {error: error}});
                    })
}


export const removeService = (id) => async(dispatch, getState) => {
        
    dispatch({type: serviceActionTypes.SET_SERVICE_AS_BUSY, payload: {busyId: id}});

    await api.removeAsync(id)
                    .catch(error => {
                        dispatch({type: serviceActionTypes.FETCH_SERVICES_FAILURE, payload: {error: error}});
                    })
}


export const postService = (service) => async(dispatch, getState) => {
        
    await api.postAsync(service)
                    .then(response => {
                        dispatch({type: serviceActionTypes.FETCH_SERVICE_SUCCESS, payload:{item: response}});
                    })                   
                    .catch(error => {
                        dispatch({type: serviceActionTypes.EDIT_SERVICE_FAILURE, payload: {editingError: error}});
                    })
}