import serviceEditReducer from "./service-edit-reducer";
import serviceListReducer from "./service-list-reducer";
import { applyMiddleware, combineReducers, createStore } from "redux";
import thunk from "redux-thunk";

const reducer = combineReducers({
    serviceEditReducer,
    serviceListReducer
});

const store = createStore(reducer, applyMiddleware(thunk));

export default store;
