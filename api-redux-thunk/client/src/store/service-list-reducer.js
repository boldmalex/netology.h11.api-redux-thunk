import serviceActionTypes from "./service-action-types";


const initialState = { 
    items: [],
    loading: false,
    error: null,
    busyItems: []
}


const serviceListReducer = (state = initialState, action) => {
 
    switch (action.type) {

        case serviceActionTypes.FETCH_SERVICES_REQUEST:
            return {...state, loading: true, error: null };

        case serviceActionTypes.FETCH_SERVICES_FAILURE: 
            const {error} = action.payload;
            return {...state, loading: false, error};

        case serviceActionTypes.FETCH_SERVICES_SUCCESS: 
            const {items} = action.payload;
            return {...state, items, loading:false, error: null};

        case serviceActionTypes.SET_SERVICE_AS_BUSY: 
            const {busyId} = action.payload;
            return {...state, busyItems: [...state.busyItems, busyId]};

        case serviceActionTypes.REMOVE_SERVICE: 
            const {id} = action.payload;
            return {...state, items: state.items.filter(o => o.id !== id)};

        default:
            return state;
    }
}

export default serviceListReducer;